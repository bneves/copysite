# **copysite command-line program for UNIX **

COPYSITE is a command-line program to create a local site copy from any online site and start a simple Python HTTP Server. running the local site copy.

Run the command:
```
#!shell
copysite www.site.com
```
You can to access the local site copy using http://localhost:1024/www.yoursitecopy.com in your web browser.


**It requires the Python interpreter, version 2.6, 2.7, or 3.2+ and wget installed.**


# **copysite URL** #

### Dependencies ###

* Python 
* Wget

# Install #

To install it right away for all UNIX users (Linux, OS X, etc.), copy and paste on terminal:


```
#!shell

sudo wget https://raw.githubusercontent.com/sevenb/copysite/master/copysite -O /usr/local/bin/copysite
sudo chmod a+rx /usr/local/bin/copysite
```

or 


```
#!shell

sudo curl -L https://raw.githubusercontent.com/sevenb/copysite/master/copysite -o /usr/local/bin/copysite
sudo chmod a+rx /usr/local/bin/copysite
```